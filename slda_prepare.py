import pandas as pd
import numpy as np
import math
from pymongo import MongoClient
import sys
import operator
import matplotlib.pyplot as plt
client = MongoClient('mongodb://localhost:27017/')


cat2map=dict()
cat2range=dict()
posphonelist=list()

def read_cat():
	global start
	print 'begin read cat'
	db=client.trafficking
	cats=db.cats.find()
	for cat in cats:
		cat2map[cat['cat']]=dict()
		cat2map[cat['cat']]['dim']=cat['dim']
		cat2map[cat['cat']]['map']=cat['map']

	print 'end read cat, len : '+str(len(cat2map))

	
	
	

def read_range():
	global start
	print 'begin read range'
	db=client.trafficking
	ranges=db.ranges.find()
	for ra in ranges:
		cat2range[ra['cat']]=dict()
		cat2range[ra['cat']]['range']=ra['range']

	print 'end read range, len : '+str(len(cat2range))


def read_pos():
	global posphonelist
	print 'begin read pos'
	db=client.trafficking
	posphones=db.posphones.find()
	posphonelist=[posphone['phone'] for posphone in posphones]
	print 'end read pos, len : '+str(len(posphonelist))


def decode_cat_feature(feature,cat):
	flist=[0]
	for k,v in feature.items():
		index=cat2map[cat]['map'][k]
		flist[0]=index+1
	return flist

def decode_num_feature(feature,cat):
	if cat=='Perspective_1st' or cat=='Perspective_3rd':
		flist=[0]
		if 'min' in feature:
			ra=cat2range[cat]['range']
			i=0
			while i<len(ra['min']):
				if feature['min']<ra['min'][i]:
					break
				i+=1
			flist[0]=i+1

	else:
		flist=[0]*3
		if 'min' in feature:
			ra=cat2range[cat]['range']
			for k,v in ra.items():
				i=0
				while i<len(v):
					if feature[k]<v[i]:
						break
					i+=1
				if k=='min':
					flist[0]=i+1
				elif k=='mean':
					flist[1]=i+1
				else:
					flist[2]=i+1
			
	return flist


def gen_feature(feature):
	flist=[]
	for k,v in cat2map.items():
		flist+=decode_cat_feature(feature[k],k)
	for k,v in cat2range.items():
		flist+=decode_num_feature(feature[k],k)
		
	
	return flist


def gen_pos():
	global posphonelist
	print 'begin gen pos'
	fwrite=open('matrix_slda/phone2matrix_pos.txt','w')
	db=client.trafficking
	#print posphonelist
	for phone in posphonelist:
		print phone
		obj=db.phones.find_one({'phone':phone})
		if not obj:
			continue
		fwrite.write("#%s#%s\n" % (phone,str(obj['ads_len'])))
		ads=obj['ads']
		for ad in ads:
			feature=db.ads.find_one({'ad_id':ad})
			feature=gen_feature(feature)
			for f in feature:
				fwrite.write("%s " % f)
			fwrite.write('\n')
	print 'end gen pos'

def plot_clusters():
	global posphonelist
	db=client.trafficking
	ax=plt.subplot(111)
	ax.set_title('Size of clusters')
	ax.set_yscale('log')
	ax.set_xlabel('Index')
	ax.set_ylabel('Size')

	print 'begin retrieval'
	y=[]
	cnt=0
	#print posphonelist
	phones=db.phones.find()

	for phone in phones:
		y.append(phone['ads_len'])
		cnt+=1
		if cnt%100==0:
			print cnt
	print 'end retrieval'
	
	
	y.sort(reverse=True)
	
	ax.plot(range(len(y)),y)
	plt.savefig('gibbs/size.png')
	plt.close()

def gen_neg(min_ads_len):
	global posphonelist
	print 'begin gen neg'
	fwrite=open('matrix_slda/phone2matrix_neg.txt','w')
	db=client.trafficking
	print posphonelist
	phones=db.phones.find({'ads_len':{'$gt':min_ads_len}})
	for phone in phones:
		print phone['phone']
		if phone['phone'] in posphonelist:
			continue
		fwrite.write("#%s#%s\n" % (phone['phone'],str(phone['ads_len'])))
		ads=phone['ads']
		for ad in ads:
			feature=db.ads.find_one({'ad_id':ad})
			feature=gen_feature(feature)
			for f in feature:
				fwrite.write("%s " % f)
			fwrite.write('\n')
	print 'end gen neg'





def gen_dim():
	start=0
	fwrite=open('matrix_slda/dim.txt','w')
	for k,v in cat2map.items():
		fwrite.write("%s,%s,%s,%s\n" % (k,str(v['dim']),str(start),str(start+1-1)))
		start+=1
	
	for k,v in cat2range.items():
		if k=='Perspective_1st' or k=='Perspective_3rd':
			fwrite.write("%s,%s,%s,%s\n" % (k,str(1),str(start),str(start+1-1)))
			start+=1
		else:
			fwrite.write("%s,%s,%s,%s\n" % (k,str(3),str(start),str(start+3-1)))
			start+=3


def gen_svm_pos():
	fread=open('matrix_slda/phone2matrix_pos.txt','r')
	fwrite=open('matrix_slda/phone2matrix_pos_svm.txt','w')
	cluster=0
	for line in fread:
		line=line.strip().split('#')
		if len(line)==3:
			cluster=float(line[1])/float(1e10)
			continue
		line=line[0].strip().split()
		fwrite.write("%s" % str(cluster))
		for i,f in enumerate(line):
			if f=='0':
				continue
			fwrite.write(" %s:%s" % (str(i+1),f))
		fwrite.write('\n')

def gen_svm_neg():
	fread=open('matrix_slda/phone2matrix_neg.txt','r')
	fwrite=open('matrix_slda/phone2matrix_neg_svm.txt','w')
	cluster=0
	for line in fread:
		line=line.strip().split('#')
		if len(line)==3:
			cluster=float(line[1])/float(1e10)
			continue
		line=line[0].strip().split()
		fwrite.write("%s" % str(cluster))
		for i,f in enumerate(line):
			if f=='0':
				continue
			fwrite.write(" %s:%s" % (str(i+1),f))
		fwrite.write('\n')





if __name__ == "__main__":
	read_cat()
	read_range()
	read_pos()
	gen_dim()
	gen_pos()
	gen_neg(50)
	gen_svm_pos()
	gen_svm_neg()
	plot_clusters()
	client.close()

