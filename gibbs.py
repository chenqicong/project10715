import numpy as np
import math
import operator
from pymongo import MongoClient
import matplotlib.pyplot as plt
client = MongoClient('mongodb://localhost:27017/')

def words():
	fwrite=open('gibbs/words.txt','w')
	words=set()
	fread=open('matrix_slda/phone2matrix_pos_svm.txt','r')
	cnt=0
	for line in fread:
		line=line.strip().split()
		for word in line[1:]:
			words.add(word)
		cnt+=1
	fread.close()
	fread=open('matrix_slda/phone2matrix_neg_svm.txt','r')
	for line in fread:
		line=line.strip().split()
		for word in line[1:]:
			words.add(word)
	fread.close()
	for word in words:
		fwrite.write('%s\n' % word)
	fwrite.close()

	return cnt



def gibbs():
	#read data
	words=[]
	fread=open('gibbs/words.txt','r')
	for line in fread:
		word=line.strip()
		words.append(word)
	fread.close()
	W=len(words)
	print "words : "+str(W)
	docs=[]
	ys=[]
	fread=open('matrix_slda/phone2matrix_pos_svm.txt','r')
	for line in fread:
		doc=[]
		line=line.strip().split()
		ys.append(float(line[0]))
		for word in line[1:]:
			item=dict()
			item['word']=words.index(word)
			item['topic']=0
			doc.append(item)
		docs.append(doc)
	fread.close()
	fread=open('matrix_slda/phone2matrix_neg_svm.txt','r')
	for line in fread:
		doc=[]
		line=line.strip().split()
		ys.append(float(line[0]))
		for word in line[1:]:
			item=dict()
			item['word']=words.index(word)
			item['topic']=0
			doc.append(item)
		docs.append(doc)
	fread.close()
	D=len(docs)
	print "docs : "+str(D)
	#set para
	T=10
	alpha=float(50)/T
	eta=0.01
	nround=250
	nsample=0
	sample_lag=10
	burn_in=50
	a=1
	b=np.linspace(0,1,T)
	#set counter
	nD=[0]*D
	nT=[0]*T
	nDT=[]
	for i in range(D):
		nDT.append([0]*T)
	nTW=[]
	for i in range(T):
		nTW.append([0]*W)
	theta=[]
	for i in range(D):
		theta.append([0]*T)
	phi=[]
	for i in range(T):
		phi.append([0]*W)

	#init
	for d,doc in enumerate(docs):
		for edge in doc:
			w=edge['word']
			t=np.random.randint(0,T)
			nD[d]+=1
			nDT[d][t]+=1
			nT[t]+=1
			nTW[t][w]+=1
			edge['topic']=t
	#gibbs
	for r in range(nround):
		for d,doc in enumerate(docs):
			for edge in doc:
				t=edge['topic']
				w=edge['word']
				nD[d]-=1
				nDT[d][t]-=1
				nT[t]-=1
				nTW[t][w]-=1
				probs=[0]*T
				for z in range(T):
					left=(nDT[d][z]+alpha)*(nTW[z][w]+eta)/float(nT[z]+W*eta)
					distribution=np.array(nDT[d])
					distribution=distribution/float(sum(distribution))
					content=2*b[z]/float(nD[d])*(ys[d]-np.dot(b,distribution)-a)-math.pow((b[z]/float(nD[d])),2)
					right=np.exp(content)
					probs[z]=left*right
				probs=np.array(probs)
				probs=probs/float(sum(probs))

				sample=list(np.random.multinomial(1,probs))
				t=sample.index(1)
				nD[d]+=1
				nDT[d][t]+=1
				nT[t]+=1
				nTW[t][w]+=1
				edge['topic']=t
		if r>burn_in:
			if r%sample_lag==0:
				for d in range(D):
					for t in range(T):
						theta[d][t]+=(nDT[d][t]+alpha)/float(nD[d]+T*alpha)
				for t in range(T):
					for w in range(W):
						phi[t][w]+=(nTW[t][w]+eta)/float(nT[t]+W*eta)
				nsample+=1
		print r
	for d in range(D):
		for t in range(T):
			theta[d][t]=theta[d][t]/float(nsample)
	for t in range(T):
		for w in range(W):
			phi[t][w]=phi[t][w]/float(nsample)


	#output
	fwrite=open('gibbs/doc2topics.txt','w')
	for d in range(D):
		for t in range(T):
			fwrite.write('%s ' % str(theta[d][t]))
		fwrite.write('\n')
	fwrite.close()
	fwrite=open('gibbs/topic2words.txt','w')
	for t in range(T):
		for w in range(W):
			fwrite.write('%s ' % str(phi[t][w]))
		fwrite.write('\n')
	fwrite.close()


def get_max_indices(flist,num):
	ret=[]
	cpflist=list(flist)
	cpflist.sort(reverse=True)
	for f in cpflist[:num]:
		ret.append(flist.index(f))
	return ret


def read_itprt():
	itprt=dict()
	cnt=1
	print 'begin read cat'
	db=client.trafficking
	cats=db.cats.find()
	for cat in cats:
		itprt[cnt]=dict()
		itprt[cnt]['cat']=cat['cat']
		itprt[cnt]['map']=dict()
		for k,v in cat['map'].items():
			itprt[cnt]['map'][v+1]=k
		cnt+=1
	print 'end read cat, len : '+str(len(itprt))

	ttypes=['min','mean','range']
	print 'begin read range'
	db=client.trafficking
	ranges=db.ranges.find()
	for ra in ranges:
		cat=ra['cat']
		rg=ra['range']
		for ttype in ttypes:
			itprt[cnt]=dict()
			itprt[cnt]['cat']=cat+"_"+ttype
			itprt[cnt]['map']=dict()
			for i,item in enumerate(rg[ttype]):
				if i==0:
					itprt[cnt]['map'][i+1]="x<"+str(rg[ttype][i])
				else:
					itprt[cnt]['map'][i+1]=str(rg[ttype][i-1])+"<x<"+str(rg[ttype][i])
			itprt[cnt]['map'][len(rg[ttype])+1]="x>"+str(rg[ttype][-1])
			cnt+=1
			if cat=="Perspective_1st" or cat=="Perspective_3rd":
				break
	print 'end read range, len : '+str(len(itprt))

	#swap 3&4
	tmp=itprt[3]
	itprt[3]=itprt[4]
	itprt[4]=tmp
	return itprt

def result(pos_ads_num):
	itprt=read_itprt()
	T=10
	words=[]
	fread=open('gibbs/words.txt','r')
	for line in fread:
		word=line.strip()
		words.append(word)
	fread.close()
	doc2topics=[]
	fread=open('gibbs/doc2topics.txt','r')
	for line in fread:
		line=line.strip().split()
		features=[float(f) for f in line]
		doc2topics.append(features)
	fread.close()
	topic2words=[]
	fread=open('gibbs/topic2words.txt','r')
	for line in fread:
		line=line.strip().split()
		features=[float(f) for f in line]
		indices=get_max_indices(features,50)
		topic2words.append([words[ind] for ind in indices])
	fread.close()
	###cluster####
	cluster2topics=dict()
	cluster_pos=set()
	cluster_neg=set()
	fread=open('matrix_slda/phone2matrix_pos_svm.txt','r')
	for line in fread:
		line=line.strip().split()
		cluster=float(line[0])
		if cluster not in cluster_pos:
			cluster_pos.add(cluster)
			cluster2topics[cluster]=dict()
			cluster2topics[cluster]['num']=0
			cluster2topics[cluster]['topics']=np.array([0]*T)
	fread.close()
	fread=open('matrix_slda/phone2matrix_neg_svm.txt','r')
	for line in fread:
		line=line.strip().split()
		cluster=float(line[0])
		if cluster not in cluster_neg:
			cluster_neg.add(cluster)
			cluster2topics[cluster]=dict()
			cluster2topics[cluster]['num']=0
			cluster2topics[cluster]['topics']=np.array([0]*T)
	fread.close()

	cnt=0
	fread=open('matrix_slda/phone2matrix_pos_svm.txt','r')
	for line in fread:
		line=line.strip().split()
		cluster=float(line[0])
		distribution=np.array(doc2topics[cnt])
		cluster2topics[cluster]['topics']=cluster2topics[cluster]['topics']+distribution
		cluster2topics[cluster]['num']+=1
		cnt+=1
	fread.close()
	fread=open('matrix_slda/phone2matrix_neg_svm.txt','r')
	for line in fread:
		line=line.strip().split()
		cluster=float(line[0])
		distribution=np.array(doc2topics[cnt])
		cluster2topics[cluster]['topics']=cluster2topics[cluster]['topics']+distribution
		cluster2topics[cluster]['num']+=1
		cnt+=1
	fread.close()

	fwrite=open('gibbs/cluster2features.txt','w')
	for cluster,topics in cluster2topics.items():
		label=""
		if cluster in cluster_pos:
			label="+"
		if cluster in cluster_neg:
			label="-"
		size=cluster2topics[cluster]['num']
		cluster2topics[cluster]['topics']=cluster2topics[cluster]['topics']/float(size)
		topics=list(cluster2topics[cluster]['topics'])
		topic=topics.index(max(topics))
		fwrite.write("#%s#%s#%s#%s#\n" % (str(cluster),label,str(size),topic))
	fwrite.close()
	
	fwrite=open('gibbs/topic2features.txt','w')
	for topic in topic2words:
		for word in topic:
			word=word.strip().split(":")
			cat=int(word[0])
			content=int(word[1])
			if itprt[cat]['cat']=="AreaCode_State" or itprt[cat]['cat']=="state":
				continue
			fwrite.write("(%s,%s) " % (itprt[cat]['cat'].encode('utf-8'),itprt[cat]['map'][content].encode('utf-8')))
		fwrite.write('\n')
	fwrite.close()	

	fwrite=open('gibbs/topic2features_number.txt','w')
	for topic in topic2words:
		for word in topic:
			fwrite.write("(%s) " % (word))
		fwrite.write('\n')
	fwrite.close()	


	
	fread=open('gibbs/cluster2features.txt','r')
	clist=[]
	for line in fread:
		line=line.strip().split('#')
		item=dict()
		item['name']=float(line[1])
		item['label']=line[2]
		item['size']=int(line[3])
		item['topic']=int(line[4])
		clist.append(item)
	fread.close()
	clist.sort(key=operator.itemgetter('size'),reverse=True)

	cnt=0
	ax=plt.subplot(211)
	ax.set_title('Positive Clusters')
	#ax.set_xlabel('topic')
	ax.set_ylabel('Probability')
	for c in clist:
		if c['label']=="+":
			ax.plot(range(T),cluster2topics[c['name']]['topics'])
			cnt+=1
			if cnt==10:
				break
	cnt=0
	ax=plt.subplot(212)
	ax.set_title('Negative Clusters')
	ax.set_xlabel('Topic')
	ax.set_ylabel('Probability')
	for c in clist:
		if c['label']=="-":
			ax.plot(range(T),cluster2topics[c['name']]['topics'])
			cnt+=1
			if cnt==10:
				break
	plt.savefig('gibbs/clusterview.png')
	plt.close()



	topic2adsstat=[]
	for i in range(T):
		topic2adsstat.append(dict(pos=0,neg=0))
	for i,topics in enumerate(doc2topics):
		topic=topics.index(max(topics))
		if i<pos_ads_num:
			topic2adsstat[topic]['pos']+=1
		else:
			topic2adsstat[topic]['neg']+=1
	topic2clustersstat=[]
	for i in range(T):
		topic2clustersstat.append(dict(pos=0,neg=0))
	for c in clist:
		label=c['label']
		topic=c['topic']
		if label=="+":
			topic2clustersstat[topic]['pos']+=1
		else:
			topic2clustersstat[topic]['neg']+=1
	print topic2clustersstat

	ax=plt.subplot(111)
	ax.set_title('Distribution from topic view')
	ax.set_xlabel('Topic')
	ax.set_ylabel('Proportion of positive')
	y=[t['pos']/float(t['pos']+t['neg']) for t in topic2adsstat]
	ax.plot(range(T),y,label="Ads")
	for i,t in enumerate(topic2clustersstat):
		if t['pos']+t['neg']>10:
			if i==0:
				ax.plot(i,t['pos']/float(t['pos']+t['neg']),'ro',label="Clusters")
			else:
				ax.plot(i,t['pos']/float(t['pos']+t['neg']),'ro')
	# y=[ for t in topic2clustersstat]
	# ax.plot(range(T),y,label="clusters")
	# plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
 #           ncol=2, mode="expand", borderaxespad=0.)
	plt.legend()
	plt.savefig('gibbs/topicview.png')
	plt.close()

	ax=plt.subplot(111)
	ax.set_title('Size of clusters')
	ax.set_yscale('log')
	pos_cnt=0
	neg_cnt=0
	for i,c in enumerate(clist):
		if c['label']=="+":
			if pos_cnt==0:
				ax.plot(i,c['size'],'go',label="Positive")
			else:
				ax.plot(i,c['size'],'go')
			pos_cnt+=1
		else:
			if neg_cnt==0:
				ax.plot(i,c['size'],'rx',label="Negative")
			else:
				ax.plot(i,c['size'],'rx')
			neg_cnt+=1
	# plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
 #           ncol=2, mode="expand", borderaxespad=0.)
	plt.legend()
	plt.savefig('gibbs/size.png')
	plt.close()

#read words
pos_ads_num=words()
#gibbs sampling
gibbs()
#plot result
result(pos_ads_num)
client.close()











