# Human Trafficking Detection #

Thanks for the effort of Maria, William and Qicong. See our
[Paper](https://onedrive.live.com/view.aspx?cid=1FD5B5D6CF114551&resid=1FD5B5D6CF114551%21142&app=WordPdf)
[Slides](https://onedrive.live.com/view.aspx?cid=1FD5B5D6CF114551&resid=1FD5B5D6CF114551%21143&app=WordPdf)

### How do I get set up? ###

```
#!bash
#install mongodb
brew update
brew install mongodb
#launch mongod
sudo mongod
#install pymongo
sudo pip install pymongo
#install pandas
sudo pip install pandas
#extract feature into mongodb, restrict max ads number to be 100000
cd project10715
python parser.py 100000
#from mongodb generate matrix for cca
python cca_prepare.py
#from mongodb generate matrix for slda
python slda_prepare.py
#run slda analysis  to get results
python gibbs.py
```

### How do I rebuild database? ###

```
#!bash
mongo
>use trafficking
>db.dropDatabase()
>exit
#and run "python parser.py <maybe larger ads number>" 
```