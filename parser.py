import pandas as pd
import numpy as np
import math
from pymongo import MongoClient
import sys
import operator
client = MongoClient('mongodb://localhost:27017/')
ad2features=dict()
cat2map=dict()
phone2ads=dict()
posphone2ads=dict()
cat2range=dict()

def is_null(obj):
	if type(obj)==float:
		return math.isnan(obj)
	else:
		return False

def get_cat_feature(obj,cat):
	flist=dict()
	if not is_null(obj):
		obj=obj.strip().split(';')
		for o in obj:
			if o=='none' or '.' in o :
				continue 
			if cat not in cat2map:
				cat2map[cat]=dict()
			if o not in cat2map[cat]:
				cat2map[cat][o]=len(cat2map[cat])
			#pos=cat2map[cat][o]
			#add pos count to flist
			if o not in flist:
				flist[o]=1
			else:
				flist[o]+=1
	return flist

def get_age_feature(obj,cat):
	flist=dict()
	if not is_null(obj):
		obj=obj.strip().split(';')
		obj=[int(o) for o in obj if o.isdigit() and int(o)>0 and int(o)<100]
		if len(obj)>0:
			if cat not in cat2range:
				cat2range[cat]=dict()
				cat2range[cat]['min']=set()
				cat2range[cat]['mean']=set()
				cat2range[cat]['range']=set()
			flist['min']=np.min(obj)
			flist['mean']=np.mean(obj)
			flist['range']=np.max(obj)-np.min(obj)
			cat2range[cat]['min'].add(flist['min'])
			cat2range[cat]['mean'].add(flist['mean'])
			cat2range[cat]['range'].add(flist['range'])
		
	return flist

def get_numeric_feature(obj,cat):
	flist=dict()
	if not is_null(obj):
		if cat=='Perspective_1st' or cat=='Perspective_3rd':
			obj=str(obj)
		obj=obj.strip().split(';')
		obj=[float(o) for o in obj if o!='none']
		if len(obj)>0:
			
			if cat not in cat2range:
				cat2range[cat]=dict()
				cat2range[cat]['min']=set()
				cat2range[cat]['mean']=set()
				cat2range[cat]['range']=set()

			flist['min']=np.min(obj)
			flist['mean']=np.mean(obj)
			flist['range']=np.max(obj)-np.min(obj)

			cat2range[cat]['min'].add(flist['min'])
			cat2range[cat]['mean'].add(flist['mean'])
			cat2range[cat]['range'].add(flist['range'])
		
	return flist


def mapPhone2Ads(obj,ad):
	if is_null(obj):
		return False
	obj=obj.strip().split(';')
	obj=[o for o in obj if o!='none']
	if len(obj)>0:
		for o in obj:
			if o not in phone2ads:
				phone2ads[o]=set()
			phone2ads[o].add(ad)
		return True
	else:
		return False

def mapPosPhone2Ads(obj,ad):
	if is_null(obj):
		return False
	obj=obj.strip().split(';')
	obj=[o for o in obj if o!='none']
	if len(obj)>0:
		for o in obj:
			if o not in posphone2ads:
				posphone2ads[o]=set()
			posphone2ads[o].add(ad)
		return True
	else:
		return False

def phonemap2list(obj):
	ret=[]
	for phone,ads in obj.items():
		item=dict()
		item['phone']=phone
		item['ads']=list(ads)
		item['ads_len']=len(ads)
		ret.append(item)
	ret.sort(key=operator.itemgetter('ads_len'),reverse=True)
	return ret

def parseData(ads_num):
	max_ad_id=0
	db=client.trafficking
	
	header=pd.read_csv('../data/header.csv')
	print 'begin reading'
	df=pd.read_csv('../data/ex.csv',names=header.columns)
	#df=pd.read_csv('../Data/sample_all.csv')
	print 'end reading'

	print 'begin ads'
	i=0
	while i<df.index.size:
		line=df.iloc[i]
		if i>ads_num:
			max_ad_id=line['ad_id']
			break	
		#PhoneNumber
		PhoneNumber=line['PhoneNumber']
		if not mapPhone2Ads(PhoneNumber,line['ad_id']):
			i+=1
			if i%100000==0:
				print i		
			continue			

		#ad_id
		ad_id=line['ad_id']
		ad2features[ad_id]=dict()
		#state
		state=line['state']
		ad2features[ad_id]['state']=get_cat_feature(state,'state')
		#print ad2features[ad_id]['state']

		#Perspective_1st
		Perspective_1st=line['Perspective_1st']
		ad2features[ad_id]['Perspective_1st']=get_numeric_feature(Perspective_1st,'Perspective_1st')
		#Perspective_3rd
		Perspective_3rd=line['Perspective_3rd']
		ad2features[ad_id]['Perspective_3rd']=get_numeric_feature(Perspective_3rd,'Perspective_3rd')

		#Age
		Age=line['Age']
		ad2features[ad_id]['Age']=get_age_feature(Age,'Age')

		#Height_ft
		Height_ft=line['Height_ft']
		ad2features[ad_id]['Height_ft']=get_numeric_feature(Height_ft,'Height_ft')
		#Height_in
		Height_in=line['Height_in']
		ad2features[ad_id]['Height_in']=get_numeric_feature(Height_in,'Height_in')
		#Weight
		Weight=line['Weight']
		ad2features[ad_id]['Weight']=get_numeric_feature(Weight,'Weight')
		#Chest
		Chest=line['Chest']
		ad2features[ad_id]['Chest']=get_numeric_feature(Chest,'Chest')
		#Waist
		Waist=line['Waist']
		ad2features[ad_id]['Waist']=get_numeric_feature(Waist,'Waist')
		#Hip
		Hip=line['Hip']
		ad2features[ad_id]['Hip']=get_numeric_feature(Hip,'Hip')

		#Ethnicity
		Ethnicity=line['Ethnicity']
		ad2features[ad_id]['Ethnicity']=get_cat_feature(Ethnicity,'Ethnicity')
		#SkinColor
		SkinColor=line['SkinColor']
		ad2features[ad_id]['SkinColor']=get_cat_feature(SkinColor,'SkinColor')
		#EyeColor
		EyeColor=line['EyeColor']
		ad2features[ad_id]['EyeColor']=get_cat_feature(EyeColor,'EyeColor')
		#HairColor
		HairColor=line['HairColor']
		ad2features[ad_id]['HairColor']=get_cat_feature(HairColor,'HairColor')
		#Restriction_Ethnicity
		Restriction_Ethnicity=line['Restriction_Ethnicity']
		ad2features[ad_id]['Restriction_Ethnicity']=get_cat_feature(Restriction_Ethnicity,'Restriction_Ethnicity')

		#Age
		Restriction_Age=line['Restriction_Age']
		ad2features[ad_id]['Restriction_Age']=get_age_feature(Restriction_Age,'Restriction_Age')

		#AreaCode_State
		AreaCode_State=line['AreaCode_State']
		ad2features[ad_id]['AreaCode_State']=get_cat_feature(AreaCode_State,'AreaCode_State')
		


		item=ad2features[ad_id]
		item['ad_id']=ad_id
		db.ads.insert(item)





		i+=1
		if i%100000==0:
			print i

	if max_ad_id==0:
		max_ad_id=df.iloc[i-1]['ad_id']
		
	print 'end ads'

	print 'begin cats'
	#cats
	for cat,mmap in cat2map.items():
		print cat+":"+str(len(mmap))
		item=dict()
		item['cat']=cat
		item['dim']=len(mmap)
		item['map']=mmap
		db.cats.insert(item)
	print 'end cats'
	print 'begin ranges'
	#range
	for cat,ra in cat2range.items():
		print cat
		item=dict()
		item['cat']=cat
		item['range']=dict()
		for k,v in ra.items():
			v=list(v)
			v.sort()
			item['range'][k]=list()
			item['range'][k].append(v[int(len(v)/4)])
			item['range'][k].append(v[int(len(v)*2/4)])
			item['range'][k].append(v[int(len(v)*3/4)])
		db.ranges.insert(item)
	print 'end ranges'

	print 'begin phones'
	#phones
	
	phonelist=phonemap2list(phone2ads)
	for item in phonelist:
		db.phones.insert(item)
	print 'end phones'

	return max_ad_id

def read_trafficking(max_ad_id):
	print 'begin pos'
	db=client.trafficking
	df=pd.read_csv('../data/trafficking.csv')
	i=0
	while i<df.index.size:
		line=df.iloc[i]
		if line['ad_id']>max_ad_id:
			break
		#PhoneNumber
		PhoneNumber=line['phone_numbers']
		mapPosPhone2Ads(PhoneNumber,line['ad_id'])
		i+=1
	print 'end pos'
		

	#pos-phones
	posphonelist=phonemap2list(posphone2ads)
	for item in posphonelist:
		db.posphones.insert(item)

			


if __name__ == "__main__":
	ads_num=int(sys.argv[1])
	max_ad_id=parseData(ads_num)
	print "max_ad_id : "+str(max_ad_id)
	read_trafficking(max_ad_id)
	client.close()

